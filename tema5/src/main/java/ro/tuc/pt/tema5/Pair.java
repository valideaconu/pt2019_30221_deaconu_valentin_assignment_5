/**
 * 
 */
package ro.tuc.pt.tema5;

/**
 * @author Vali
 *
 */
public class Pair<E, T> {
	private E first;
	private T second;
	
	public Pair() {
		this.first = null;
		this.second = null;
	}
	
	public Pair(E first, T second) {
		this.first = first;
		this.second = second;
	}

	public E getFirst() {
		return first;
	}

	public void setFirst(E first) {
		this.first = first;
	}

	public T getSecond() {
		return second;
	}

	public void setSecond(T second) {
		this.second = second;
	}

	public boolean equals(Pair<E, T> obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (first == null) {
			if (obj.first != null)
				return false;
		} else if (!first.equals(obj.first))
			return false;
		if (second == null) {
			if (obj.second != null)
				return false;
		} else if (!second.equals(obj.second))
			return false;
		return true;
	}
	
	
}
