package ro.tuc.pt.tema5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Vali
 *
 */
public class Computer {

	public static int countDistinctDays(ArrayList<MonitoredData> data) {
		ArrayList<Pair<Integer, Integer>> freq = new ArrayList<Pair<Integer, Integer>>();
		
		for (MonitoredData md : data) {
			int[] day = new int[] {
				Integer.parseInt(md.getStartTime().getDay()),
				Integer.parseInt(md.getEndTime().getDay()),								
			};

			int[] month = new int[] {
				Integer.parseInt(md.getStartTime().getMonth()),
				Integer.parseInt(md.getEndTime().getMonth()),								
			};

			Pair<Boolean, Boolean> q = new Pair<Boolean, Boolean>(false, false);
			Pair<Integer, Integer> day1 = new Pair<Integer, Integer>(day[0], month[0]);
			Pair<Integer, Integer> day2 = new Pair<Integer, Integer>(day[1], month[1]);
			
			for (Pair<Integer, Integer> p : freq) {
				if (p.equals(day1)) {
					q.setFirst(true);
				}
				
				if (p.equals(day2)) {
					q.setSecond(true);
				}
			}
			
			if (q.getFirst() == false) {
				freq.add(day1);
			}
			
			if (q.getSecond() == false) {
				freq.add(day2);
			}
		}
		
		return freq.size() - 1;
	}
	
	public static HashMap<String, Integer> computeMapForDistinctActions(ArrayList<MonitoredData> data) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		
		for (MonitoredData md : data) {
			String key = md.getActivity();
			if (map.containsKey(key)) {
				int current = map.get(key);
				map.put(key, current + 1);
			} else {
				map.put(key, 1);
			}
		}
		
		String content = new String("Computing map for distinct actions , result : \n");
		for (Map.Entry<String, Integer> e : map.entrySet())
			content += (" [ " + e.getKey() + " ] " + e.getValue() + "\n");	
		
		TextWriter("distinct_actions_result.txt", content);
		
		return map;
	}
	
	public static HashMap<Integer, HashMap<String, Integer> > computeMapForDistinctDays(ArrayList<MonitoredData> data) {
		HashMap<Integer, HashMap<String, Integer> > map = new HashMap<Integer, HashMap<String, Integer> >();
		
		for (MonitoredData md : data) {
			int key = Integer.parseInt(md.getStartTime().getMonth()) * 100 + Integer.parseInt(md.getEndTime().getDay());
			if (map.containsKey(key)) {
				HashMap<String, Integer> value = map.get(key);
				String valueKey = md.getActivity();
				if (value.containsKey(valueKey)) {
					int current = value.get(valueKey);
					value.put(valueKey, current + 1);
				} else {
					value.put(valueKey, 1);
				}
			} else {
				map.put(key, new HashMap<String, Integer>());
			}
		}
		
		String content = new String("Computing map for distinct days , result : \n");
		for (Map.Entry<Integer, HashMap<String, Integer>> iterator : map.entrySet()) {
			content += (" [ Date : " + iterator.getKey() % 100 + " / " + iterator.getKey() / 100 + " ] : \n");
			for (Map.Entry<String, Integer> e : iterator.getValue().entrySet())
				content += (" < " + e.getKey() + " , " + e.getValue() + " > \n");			
		}
		TextWriter("distinct_days_result.txt", content);
		
		return map;
	}

	public static HashMap<String, Date> computeMapForTotalDuration(ArrayList<MonitoredData> data) {
		HashMap<String, Date> map = new HashMap<String, Date>();
		
		for (MonitoredData md : data) {
			String key = md.getActivity();
			Date itv = md.getEndTime().sub(md.getStartTime());
			if (map.containsKey(key)) {
				Date dt = map.get(key);
				map.put(key, dt.add(itv));
			} else {
				map.put(key, itv);
			}
		}
		
		String content = new String("Computing map for total duration, result: \n");
		for (Map.Entry<String, Date> e : map.entrySet()) {
			content += (" [ " + e.getKey() + " ] " + e.getValue().toStringTimePassed() + "\n");	
		}
		
		TextWriter("total_duration_result.txt", content);
		
		return map;
	}
	
	public static ArrayList<String> computeFilteredData(ArrayList<MonitoredData> data) {
		ArrayList<String> activities = new ArrayList<String>();
		ArrayList<Integer[]> totals = new ArrayList<Integer[]>();
		
		HashMap<String, Integer> map = computeMapForDistinctActions(data);
		for (Map.Entry<String, Integer> e : map.entrySet()) {
			activities.add(e.getKey());
		}
		
		for (String act : activities) {
			Integer[] d = new Integer[] {0, 0};
			for (MonitoredData md : data) {
				if (md.getActivity().equals(act)) {
					d[0]++;
					Date itv = md.getEndTime().sub(md.getStartTime());
					if (Integer.parseInt(itv.getMinutes()) <= 5 && Integer.parseInt(itv.getHours()) == 0) {
						d[1]++;
					}
				}
			}
			totals.add(d);
		}

		ArrayList<String> res = new ArrayList<String>();
		for (int i = 0; i < activities.size(); ++i) {
			Integer[] d = totals.get(i);
			float m = (d[1] / (d[0] * 1.0f)) * 100.0f;
			if (m >= 90.0f) {
				res.add(activities.get(i));
			}
		}
		
		String content = new String("Computing list with filtered monitored data , if duration <= 5 minutes , result : \n");
		for (String act : res) {
			content += act + "\n";
		}
		
		TextWriter("total_filtered_monitored_data.txt", content);
				
		return null;
	}
	
	public static boolean TextWriter(String fileName, String content) {		
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(fileName));
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			System.out.println("Error occurs: " + e.getMessage());
			return false;
		}
		
		return true;
	}
}
