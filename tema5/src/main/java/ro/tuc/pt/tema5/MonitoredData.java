package ro.tuc.pt.tema5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Vali
 *
 */
public class MonitoredData {
	private static final String DATA_FILE_NAME = new String("Activities.txt"); 
	private static final Pair<Integer, Integer> START_TIME_INTERVAL = new Pair<Integer, Integer>(0, 19);
	private static final Pair<Integer, Integer> END_TIME_INTERVAL = new Pair<Integer, Integer>(21, 40);
	private static final int ACTIVITY_NAME_POSITION = 42;
	
	
	private Date startTime;
	private Date endTime;
	private String activity;
	
	public MonitoredData(Date startTime, Date endTime, String activity) {
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}
	
	public MonitoredData(String input) {
		String tmp = input.substring(ACTIVITY_NAME_POSITION);
		int n = tmp.length();
		int lastLetterPosition = -1;
		for (int i = n - 1; i >= 0; --i) {
			if (Character.isLetter(tmp.charAt(i))) {
				lastLetterPosition = i;
				break;
			}
		}
		
		this.activity = tmp.substring(0, lastLetterPosition + 1);
		this.startTime = new Date(input.substring(START_TIME_INTERVAL.getFirst(), START_TIME_INTERVAL.getSecond()));
		this.endTime = new Date(input.substring(END_TIME_INTERVAL.getFirst(), END_TIME_INTERVAL.getSecond()));
	
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the activity
	 */
	public String getActivity() {
		return activity;
	}

	/**
	 * @param activity the activity to set
	 */
	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	public static ArrayList<MonitoredData> uploadData() {
		List<MonitoredData> data = new ArrayList<MonitoredData>();
		
		try (Stream<String> stream = Files.lines(Paths.get(DATA_FILE_NAME))) {
			data = stream
					.map(line -> new MonitoredData(line))
					.collect(Collectors.toList());
		} catch (IOException e) {
			System.out.println("Error occurs: " + e.getMessage());
		} 
		
		return (ArrayList<MonitoredData>)data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new String(startTime + "\t" + endTime + "\t" + activity);
	}
	
}
