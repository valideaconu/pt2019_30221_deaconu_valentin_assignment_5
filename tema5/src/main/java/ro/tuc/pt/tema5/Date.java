/**
 * 
 */
package ro.tuc.pt.tema5;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Vali
 *
 */
public class Date {
	// YYYY-MM-DD H:m:s == 2011-11-28 10:18:11
	private static final Pattern DATE_PATTERN = Pattern.compile("(\\d+)-(\\d+)-(\\d+) (\\d+):(\\d+):(\\d+)");

	private int year;
	private int month;
	private int day;
	
	private int hours;
	private int minutes;
	private int seconds;
	
	public Date() {
		year = 0;
		month = 0;
		day = 0;
		
		hours = 0;
		minutes = 0;
		seconds = 0;
	}
	
	public Date(String date) {
		Matcher m = DATE_PATTERN.matcher(date);
		if (m.matches()) {
			year = Integer.parseInt(m.group(1));
			month = Integer.parseInt(m.group(2));
			day = Integer.parseInt(m.group(3));
			
			hours = Integer.parseInt(m.group(4));
			minutes = Integer.parseInt(m.group(5));
			seconds = Integer.parseInt(m.group(6));
		}
	}

	/**
	 * @return the year
	 */
	public String getYear() {
		return String.valueOf(year);
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		if (year > 999 && year < 10000)
			this.year = year;
	}

	/**
	 * @return the month
	 */
	public String getMonth() {
		String str = (month > 0 && month < 10) ? new String("0") : new String("");
		return str + month;
	}
	
	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		if (month > 0 && month < 13)
			this.month = month;
	}

	/**
	 * @return the day
	 */
	public String getDay() {
		String str = (day > 0 && day < 10) ? new String("0") : new String("");
		
		return str + day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		int[] limits = (year % 4 == 0) ? 
				new int[] {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31} : 
				new int[] {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		
		if (day > 0 && day <= limits[month])
			this.day = day;
	}

	/**
	 * @return the hours
	 */
	public String getHours() {
		String str = (hours > 0 && hours < 10) ? new String("0") : new String("");
		return str + hours;
	}

	/**
	 * @param hours the hours to set
	 */
	public void setHours(int hours) {
		if (hours >= 0 && hours < 24)
			this.hours = hours;
	}

	/**
	 * @return the minutes
	 */
	public String getMinutes() {
		String str = (minutes > 0 && minutes < 10) ? new String("0") : new String("");
		return str + minutes;
	}

	/**
	 * @param minutes the minutes to set
	 */
	public void setMinutes(int minutes) {
		if (minutes >= 0 && minutes < 60)
			this.minutes = minutes;
	}

	/**
	 * @return the seconds
	 */
	public String getSeconds() {
		String str = (seconds > 0 && seconds < 10) ? new String("0") : new String("");
		return str + seconds;
	}

	/**
	 * @param seconds the seconds to set
	 */
	public void setSeconds(int seconds) {
		if (seconds >= 0 && seconds < 60)
			this.seconds = seconds;
	}
	
	public int getDayIndex() {
		int y = year;
		int m = month;
		int d = day;
		
		int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
	   	if (m < 3)
	   		--y;
	   	
	   	int index = (y + y/4 - y/100 + y/400 + t[m - 1] + d) % 7;
	   	int result[] = {6, 0, 1, 2, 3, 4, 5};
	    return result[index];
	}
	
	public Date sub(Date dt) {
		int totalDay1 = (this.hours < dt.hours) ? (24 * 60 * 60) : 0;
		int totalDay2 = 0;

		totalDay1 += this.seconds + this.minutes * 60 + this.hours * 60 * 60;
		totalDay2 = dt.seconds + dt.minutes * 60 + dt.hours * 60 * 60;
		
		int total = totalDay1 - totalDay2;
		
		Date res = new Date();
		res.hours = (total - (total % 3600)) / 3600;
		res.minutes = (total % 3600) / 60;
		res.seconds = total - res.minutes * 60 - res.hours * 3600;
		
		return res;
	}
	
	public Date add(Date dt) {
		Date res = new Date();
		
		res.seconds = this.seconds + dt.seconds;
		if (res.seconds > 59) {
			res.seconds -= 60;
			res.minutes++;
		}
		
		res.minutes += this.minutes + dt.minutes;
		if (res.minutes > 59) {
			res.minutes -= 59;
			res.hours++;
		}
		
		res.hours += this.hours + dt.hours;
		if (res.hours > 23) {
			res.hours -= 24;
			res.day++;
		}
		
		return res;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String[] weekDays = { "Luni", "Marti", "Miercuri", "Joi", "Vineri", "Sambata", "Duminica" };
		String[] months = { "Ianuarie", "Februarie", "Martie", "Aprilie", "Mai", "Iunie", "Iulie", "August", "Septembrie", "Octombrie", "Noiembrie", "Decembrie" };
		
		String result = new String("");
		
		result += weekDays[ this.getDayIndex() ] + ", ";
		result += this.getDay() + " ";
		result += months[ this.month - 1 ] + " ";
		result += this.year + ", ";
		result += this.getHours() + ":";
		result += this.getMinutes();
		
		return result;
	}
	
	public String toStringTimePassed() {
		return new String(year + " year ( s ) , " + month + " month ( s ) , " + day + " day ( s ) , " + hours + " hour ( s ) , " + minutes + " minute ( s ) , " + seconds + " second ( s ) .");
	}
	
}
