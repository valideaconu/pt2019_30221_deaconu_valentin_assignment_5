package ro.tuc.pt.tema5;

import java.util.ArrayList;

public class App {
	public static void main(String[] args) {
		ArrayList<MonitoredData> data = new ArrayList<MonitoredData>();
		
		data = MonitoredData.uploadData();
		
		System.out.println("Distinct days: " + Computer.countDistinctDays(data));
		
		Computer.computeMapForDistinctActions(data);
		Computer.computeMapForDistinctDays(data);
		Computer.computeMapForTotalDuration(data);
		Computer.computeFilteredData(data);
		
	}
}
